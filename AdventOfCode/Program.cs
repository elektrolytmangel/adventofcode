﻿// See https://aka.ms/new-console-template for more information
using AdventOfCode.Puzzels;

Console.WriteLine("AdventOfCode 2021");
var one = new One("./Input/OneInput.txt");
one.Part1();
one.Part2();

var two = new Two("./Input/TwoInput.txt");
two.Part1();

var six = new Six("./Input/SixInput.txt");
six.Part1();
six.Part2();