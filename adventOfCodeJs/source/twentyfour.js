import { getLines } from "./modules/input.js";
console.log('------ 24 -------')
let inputValues = getLines("24");

let values = {
  w = 0,
  x = 0,
  y = 0,
  z = 0,
}

const inp = (instructions, modelNumber) => {
  let modelArr = String(modelNumber).split('').map(num => {
    return Number(num)
  });
  let v = instructions[1];
  values[v] = modelArr.shift();
  return modelArr.map(x => {
    return parseInt(String(x), 10);
  });
};

const add = (instructions) => {
  let a = instructions[1];
  let b = instructions[2];
  values[a] = values[a] + values[b];
};

const mul = (instructions) => {
  let a = instructions[1];
  let b = instructions[2];
  values[a] = values[a] * values[b];
};

const div = (instructions) => {
  let a = instructions[1];
  let b = instructions[2];
  values[a] = parseInt(values[a] / values[b]);
};

const mod = (instructions) => {
  let a = instructions[1];
  let b = instructions[2];
  values[a] = values[a] % values[b];
};

const eql = (instructions) => {
  let a = instructions[1];
  let b = instructions[2];
  values[a] = values[a] === values[b] ? 1 : 0;
};

const part1 = () => {
  let modelNumber = 13579246899999;

  for (let line of inputValues) {
    let instructions = line.split(' ');
    let cmd = instructions[0];
    switch (cmd) {
      case 'inp':
        modelNumber = inp(instructions, modelNumber);
        break;
      case 'add':
        add(instructions);
        break;
      case 'mul':
        mul(instructions);
        break;
      case 'div':
        div(instructions);
        break;
      case 'mod':
        mod(instructions);
        break;
      case 'eql':
        eql(instructions);
        break;
    }
  }

  console.log(values['z']);
};

part1();